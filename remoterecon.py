#!/usr/bin/python

###################################################
#
#   RemoteRecon - written by Justin Ohneiser
# ------------------------------------------------
# Inspired by Mike Czumak's reconscan.py
#
# This program will conduct full reconnaissance
# on a target using three steps:
#   1. Light NMAP scan -> to identify services
#   2. Modular enumeration for each service
#   3. Heavy NMAP scan
#
# [Warning]:
# This script comes as-is with no promise of functionality or accuracy.  I strictly wrote it for personal use
# I have no plans to maintain updates, I did not write it to be efficient and in some cases you may find the
# functions may not produce the desired results so use at your own risk/discretion. I wrote this script to
# target machines in a lab environment so please only use it against systems for which you have permission!!
#
# (*) Data gets logged to console and in directory named after target
# (!) Use ctrl+c to skip modules.
#
# Designed for use in Kali Linux 4.6.0-kali1-686
###################################################

import os, sys, re, urlparse
from subprocess import Popen, PIPE

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

TIMEOUT = 300
TIMEOUT_LONG = 900

# ------------------------------------
#       Printing
# ------------------------------------

def printHeader(target):
    print """
________                      _____     ________
___  __ \___________ ___________  /________  __ \_______________________
__  /_/ /  _ \_  __ `__ \  __ \  __/  _ \_  /_/ /  _ \  ___/  __ \_  __ \\
_  _, _//  __/  / / / / / /_/ / /_ /  __/  _, _//  __/ /__ / /_/ /  / / /
/_/ |_| \___//_/ /_/ /_/\____/\__/ \___//_/ |_| \___/\___/ \____//_/ /_/

                           :: %s ::
    """ % (target)

def printUsage():
    print "Usage: %s <target ip>" % sys.argv[0]

def printPlus(message):
    print bcolors.OKGREEN + "[+] " + message + bcolors.ENDC

def printStd(message):
    print bcolors.WARNING + "[*] " + message + bcolors.ENDC

def printErr(message):
    print bcolors.FAIL + "[-] " + message + bcolors.ENDC

def printInfo(message):
    print bcolors.OKBLUE + "[~] " + message + bcolors.ENDC

# ------------------------------------
#       Toolbox
# ------------------------------------

def parseNmapScan(results):
    services = {}
    lines = results.split("\n")
    for line in lines:
        ports = []
        line = line.strip()
        if (("tcp" in line and "open" in line and not "filtered" in line and not "Discovered" in line) or
            ("udp" in line and "open" in line and not "Discovered" in line)):
            while "  " in line:
                line = line.replace("  ", " ");
            linesplit = line.split(" ")
            service = linesplit[2]
            port = linesplit[0]
            if service in services:
                ports = services[service]
            ports.append(port)
            services[service] = ports
    return services

def formatPorts(ports):
    portString = ""
    for port in ports:
        port = port.split("/")[0]
        portString += "%s," % port
    return portString

def dispatchModules(target, services):
    for service in services:
        ports = services[service]
        if service in KNOWN_SERVICES:
            try:
                KNOWN_SERVICES[service](target, ports)
            except AttributeError:
                printInfo("No module available for %s - %s" % (service, ports))
        else:
            printInfo("No module available for %s - %s" % (service, ports))

def validateIP(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

def parseIP(s):
    urls = re.findall("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", s)
    for url in urls:
        url = url.lower()
    return list(set(urls))

def parseIPDirs(s):
    urls = re.findall("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+/", s)
    for url in urls:
        url = url.lower()
    return list(set(urls))

# ------------------------------------
#       Logging
# ------------------------------------

def prepareFolder(target):
    printStd("Enabling logging...")
    directory = "%s/%s" % (os.getcwd(), target)
    if not os.path.exists(directory):
        os.makedirs(directory)
        printPlus("Logging enabled: %s" % directory)
        return True
    printErr("Log for %s already exists at %s" % (target, directory))
    return False

def getFile(target, name):
    return "%s/%s/%s.txt" % (os.getcwd(), target, name)

# ------------------------------------
#       Operation
# ------------------------------------

def execute(scanName, target, moduleName, command):
    printStd("Conducting %s..." % scanName)
    process = None
    output = ""
    status = -1
    try:
        with open(getFile(target, moduleName), "a+") as file:
            file.write(
                "\n###################################################\n" +
                command +
                "\n---------------------------------------------------\n")
            process = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
            for line in iter(process.stdout.readline, ""):
                output += line
                print(line.strip())
                file.write(line)
            status = process.wait()
    except KeyboardInterrupt:
        printInfo("Skipping %s:\n\t%s" % (scanName, command))
    except Exception as e:
        printErr("Error: %s" % e)
    finally:
        try:
            process.terminate()
        except Exception:
            pass
    if status > 0:
        printErr("Unable to complete %s:\n\t%s" % (scanName, command))
    return output, status

# ------------------------------------
#       Modules
# ------------------------------------

def conductLightNmap(target):
    moduleName = "nmap_light"
    # Conduct Light TCP Scan #
    tcpScan = "nmap --host-timeout %s --open %s" % (TIMEOUT, target)
    tcpResults, tcpStatus = execute("Light TCP Scan", target, moduleName, tcpScan)
    # Conduct Light UDP Scan #
    udpScan = "nmap --host-timeout %s -sU -p 161 --open %s" % (TIMEOUT, target)
    udpResults, udpStatus = execute("Light UDP Scan", target, moduleName, udpScan)
    # Filter Results #
    services = parseNmapScan("%s\n%s" % (tcpResults, udpResults))
    return services

def conductHeavyNmap(target):
    moduleName = "nmap_heavy"
    # Conduct Heavy TCP Scan #
    tcpScan = "nmap --host-timeout %s -A --top-ports 10000 %s" % (TIMEOUT_LONG, target)
    tcpResults, tcpStatus = execute("Heavy TCP Scan", target, moduleName, tcpScan)
    # Conduct Heavy UDP Scan #
    udpScan = "nmap --host-timeout %s -sU --top-ports 100 %s" % (TIMEOUT_LONG, target)
    udpResults, udpStatus = execute("Heavy UDP Scan", target, moduleName, udpScan)

def ftp(target, ports):
    moduleName = "ftp"
    # Conduct Nmap Scan #
    nmapScripts = "ftp-vuln-*,ftp-anon"
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC --script=\"%s\" %s" % (TIMEOUT, formatPorts(ports), nmapScripts, target)
    nmapResults, nmapStatus = execute("FTP Nmap Scan", target, moduleName, nmapScan)

def ssh(target, ports):
    moduleName = "ssh"
    # Conduct Nmap Scan #
    nmapScripts = "ssh-auth-methods,ssh-hostkey,ssh2-enum-algos,sshv1"
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC --script=\"%s\" %s" % (TIMEOUT, formatPorts(ports), nmapScripts, target)
    nmapResults, nmapStatus = execute("SSH Nmap Scan", target, moduleName, nmapScan)

def smtp(target, ports):
    moduleName = "smtp"
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -p %s -sV --script=\"smtp-vuln*\" %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("SMTP Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force #
    bruteScan = "smtp-user-enum -M EXPN -U /usr/share/fern-wifi-cracker/extras/wordlists/common.txt -t %s" % (target)
    bruteResults, bruteStatus = execute("SMTP Brute Force", target, moduleName, bruteScan)

def pop3(target, ports):
    moduleName = "pop3"
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -p %s -sV --script=\"pop3-capabilities,pop3-ntlm-info\" %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("POP3 Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force #
    bruteScan = "nmap --host-timeout %s -p %s --script=\"pop3-brute\" %s" % (TIMEOUT, formatPorts(ports), target)
    bruteResults, bruteStatus = execute("POP3 Brute Force", target, moduleName, bruteScan)

def imap(target, ports):
    moduleName = "imap"
    # Conduct Basic Scan"
    nmapScan = "nmap --host-timeout %s -p %s -sV --script=\"imap-capabilities,imap-ntlm-info\" %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("IMAP Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force"
    bruteScan = "nmap --host-timeout %s -p %s --script=\"imap-brute\" %s" % (TIMEOUT, formatPorts(ports), target)
    bruteResults, bruteStatus = execute("IMAP Brute Force", target, moduleName, bruteScan)

def smb(target, ports):
    moduleName = "smb"
    # Conduct Vulnerability Scan 1 #
    nmapScan1 = "nmap --host-timeout %s -sV -sC --script=\"smb-vuln-*,samba-vuln-*\" -p 445,139 %s" %(TIMEOUT, target)
    nmap1Results, nmap1Status = execute("SMB Nmap Scan [Primary]", target, moduleName, nmapScan1)
    # Conduct Vulnerability Scan 2 #
    nmapScan2 = "nmap --host-timeout %s -sU -sV -sC --script=\"smb-vuln-*\" -p U:137,T:139 %s" % (TIMEOUT, target)
    nmap2Results, nmap2Status = execute("SMB Nmap Scan [Secondary]", target, moduleName, nmapScan2)
    # Conduct Lookup #
    lookupScan = "nmblookup -A %s" % (target)
    lookupResults, lookupStatus = execute("SMB Lookup", target, moduleName, lookupScan)
    # Conduct Enumeration #
    enumScan = "enum4linux %s" % (target)
    enumResults, enumStatus = execute("SMB Enumeration", target, moduleName, enumScan)
    advice = "use :: smbclient //<server>/<share> -I <target ip> -N :: to mount shared drive anonymously"
    printStd(advice)

def http(target, ports):
    moduleName = "http"
    # Conduct Nmap Scan #
    nmapScripts = "http-methods,http-robots.txt,http-vuln-*,http-userdir-enum,http-iis-webdav-vuln,http-majordomo2-dir-traversal,http-axis2-dir-traversal,http-tplink-dir-traversal,http-useragent-tester"
    nmapScan = "nmap --host-timeout %s -p %s -sC -sV --script=\"%s\" %s" % (TIMEOUT, formatPorts(ports), nmapScripts, target)
    nmapResults, nmapStatus = execute("HTTP Nmap Scan", target, moduleName, nmapScan)
    # Conduct WebDav Scan #
    webdavScan = "nmap --host-timeout %s -p %s --script http-webdav-scan %s -d 2>/dev/null | grep 'http-webdav-scan %s'" % (TIMEOUT, formatPorts(ports), target, target)
    webdavResults, webdavStatus = execute("HTTP WebDav Check", target, moduleName, webdavScan)
    # Stage Brute #
    for port in ports:
        urlArr = []
        urlDir = ["/"]
        port = port.split("/")[0]
        # Conduct Brute Force #
        dirbScan = "dirb http://%s:%s /usr/share/wordlists/dirb/common.txt -S -r" % (target, port)
        dirbResults, dirbStatus = execute("HTTP Brute Force against %s" % port, target, moduleName, dirbScan)
        urlArr = parseIP(dirbResults)
        urlDir = parseIPDirs(dirbResults)
        if len(urlArr) > 1:
            printPlus("Found: %s" % len(urlArr))
        # Stage Spider #
        spiderScripts = "http-shellshock,http-auth-finder,http-backup-finder,http-comments-displayer,http-config-backup,http-default-accounts,http-dombased-xss,http-errors,http-fileupload-exploiter,http-method-tamper,http-passwd,http-phpmyadmin-dir-traversal,http-phpself-xss,http-rfi-spider,http-sitemap-generator,http-sql-injection,http-stored-xss,http-unsafe-output-escaping"
        for url in urlDir:
            # Conduct Nikto Scan #
            niktoScan = "nikto -host http://%s:%s -root %s" % (target, port, urlparse.urlparse(url).path)
            niktoResults, niktoStatus = execute("HTTP Nikto Scan against %s" % url, target, moduleName, niktoScan)
            # Conduct Vulnerability Scan #
            spiderArgs = "http-shellshock.uri=%(url)s,http-backup-finder.url=%(url)s,http-config-backup.path=%(url)s,http-default-accounts.category=web,http-default-accounts.basepath=%(url)s,httpspider.url=%(url)s,http-method-tamper.uri=%(url)s,http-passwd.root=%(url)s,http-phpmyadmin-dir-traversal.dir=%(url)s,http-phpself-xss.uri=%(url)s,http-rfi-spider.url=%(url)s,http-sitemap-generator.url=%(url)s,http-sql-injection.url=%(url)s,http-unsafe-output-escaping.url=%(url)s" % {"url":urlparse.urlparse(url).path}
            vulnScan = "nmap --host-timeout %s -p %s %s --script=\"%s\" --script-args=\"%s\" 2>&1" % (TIMEOUT, port, target, spiderScripts, spiderArgs)
            vulnResults, vulnStatus = execute("HTTP Vulnerability Scan against %s" % url, target, moduleName, vulnScan)

def https(target, ports):
    moduleName = "https"
    # Conduct Nmap Scan #
    nmapScripts = "http-methods,http-robots.txt,http-vuln-*,http-shellshock,http-userdir-enum,http-iis-webdav-vuln,http-majordomo2-dir-traversal,http-axis2-dir-traversal,http-tplink-dir-traversal,http-useragent-tester,ssl-*"
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC --script=\"%s\" %s" % (TIMEOUT, formatPorts(ports), nmapScripts, target)
    nmapResults, nmapStatus = execute("HTTPS Nmap Scan", target, moduleName, nmapScan)
    # Conduct WebDav Scan #
    webdavScan = "nmap --host-timeout %s -p %s --script http-webdav-scan %s -d 2>/dev/null | grep 'http-webdav-scan %s'" % (TIMEOUT, formatPorts(ports), target, target)
    webdavResults, webdavStatus = execute("HTTPS WebDav Check", target, moduleName, webdavScan)
    # Stage Brute Force #
    for port in ports:
        urlArr = []
        urlDir = ["/"]
        port = port.split("/")[0]
        # Conduct Brute Force #
        dirbScan = "dirb https://%s:%s /usr/share/wordlists/dirb/common.txt -S -r" % (target, port)
        dirbResults, dirbStatus = execute("HTTPS Brute Force against %s" % port, target, moduleName, dirbScan)
        urlArr = parseIP(dirbResults)
        urlDir = parseIPDirs(dirbResults)
        if len(urlArr) > 1:
            printPlus("Found: %s" % len(urlArr))
        # Stage Spider #
        spiderScripts = "http-auth-finder,http-backup-finder,http-comments-displayer,http-config-backup,http-default-accounts,http-dombased-xss,http-errors,http-fileupload-exploiter,http-method-tamper,http-passwd,http-phpmyadmin-dir-traversal,http-phpself-xss,http-rfi-spider,http-sitemap-generator,http-sql-injection,http-stored-xss,http-unsafe-output-escaping"
        for url in urlDir:
            # Conduct Nikto Scan #
            niktoScan = "nikto -host https://%s:%s -root %s -ssl" % (target, port, urlparse.urlparse(url).path)
            niktoResults, niktoStatus = execute("HTTPS Nikto Scan against %s" % url, target, moduleName, niktoScan)
            # Conduct Vulnerability Scan #
            spiderArgs = "http-backup-finder.url=%(url)s,http-config-backup.path=%(url)s,http-default-accounts.category=web,http-default-accounts.basepath=%(url)s,httpspider.url=%(url)s,http-method-tamper.uri=%(url)s,http-passwd.root=%(url)s,http-phpmyadmin-dir-traversal.dir=%(url)s,http-phpself-xss.uri=%(url)s,http-rfi-spider.url=%(url)s,http-sitemap-generator.url=%(url)s,http-sql-injection.url=%(url)s,http-unsafe-output-escaping.url=%(url)s" % {"url":urlparse.urlparse(url).path}
            vulnScan = "nmap --host-timeout %s -p %s %s --script=\"%s\" --script-args=\"%s\" 2>&1" % (TIMEOUT, port, target, spiderScripts, spiderArgs)
            vulnResults, vulnStatus = execute("HTTPS Vulnerability Scan against %s" % url, target, moduleName, vulnScan)

def snmp(target, ports):
    moduleName = "snmp"
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -sU -p %s --script=\"snmp-*\" %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("SNMP Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force #
    bruteScan = "onesixtyone -c /usr/share/doc/onesixtyone/dict.txt %s 2>&1" % (target)
    bruteResults, bruteStatus = execute("SNMP Brute Force", target, moduleName, bruteScan)
    foundCount = len(bruteResults.split('\n')) - 1
    if foundCount > 1:
        walkScan = "for s in $(onesixtyone -c /usr/share/doc/onesixtyone/dict.txt %s | grep %s | cut -d ' ' -f 2 | sed -e 's/\[//g' -e 's/\]//g');do snmpwalk -c $s -v1 %s;done" % (target, target, target)
        walkResults, walkStatus = execute("SNMP Map", target, moduleName, walkScan)
    # advice = "If match community string, use :: snmpwalk -c <community string> -v1 %s :: to enumerate" % target
    # printStd(advice)

def ms_sql(target, ports):
    moduleName = "ms_sql"
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("MS-SQL Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force #
    bruteScan = "timeout %i medusa -h %s -U /usr/share/wordlists/metasploit/default_users_for_services_unhash.txt -P /usr/share/wordlists/metasploit/default_pass_for_services_unhash.txt -M mssql -L -f -v0" % (TIMEOUT, target)
    bruteResults, bruteStatus = execute("MS-SQL Brute Force", target, moduleName, bruteScan)

def mysql(target, ports):
    moduleName = "mysql"
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("MySQL Nmap Scan", target, moduleName, nmapScan)
    # Conduct Brute Force #
    bruteScan = "timeout %i medusa -h %s -U /usr/share/wordlists/metasploit/default_users_for_services_unhash.txt -P /usr/share/wordlists/metasploit/default_pass_for_services_unhash.txt -M mysql -L -f -v0" % (TIMEOUT, target)
    bruteResults, bruteStatus = execute("MySQL Brute Force", target, moduleName, bruteScan)

def nfs(target, ports):
    moduleName = "nfs"
    # Conduct Process Check #
    rpcScan = "rpcinfo -s %s" % target
    rpcResults, rpcStatus = execute("RPC Status Check", target, moduleName, rpcScan)
    # Conduct Share Enumeration #
    shareScan = "showmount -e %s" % target
    shareResults, shareStatus = execute("RPC Share Enumeration", target, moduleName, shareScan)
    if shareStatus == 0:
        advice = "Use :: mount -t ntf %s:[share] /mnt/%s -o nolock :: to mount share"
        printStd(advice)
    # Conduct Nmap Scan #
    nmapScan = "nmap --host-timeout %s -p %s -sV -sC --script=\"nfs-showmount,nfs-ls\" %s" % (TIMEOUT, formatPorts(ports), target)
    nmapResults, nmapStatus = execute("RPC Nmap Scan", target, moduleName, nmapScan)

# ------------------------------------
#       Main
# ------------------------------------

KNOWN_SERVICES = {
    "ftp"           :   ftp,
    "ssh"           :   ssh,
    "smtp"          :   smtp,
    "pop3"          :   pop3,
    "imap"          :   imap,
    "msrpc"         :   smb,
    "http"          :   http,
    "http-alt"      :   http,
    "http-proxy"    :   http,
    "https"         :   https,
    "snmp"          :   snmp,
    "ms-sql-s"      :   ms_sql,
    "mysql"         :   mysql,
    "rpcbind"       :   nfs
}

def main(argv):
    # Validate Input #
    if len(sys.argv) != 2:
        printUsage()
        sys.exit(2)
    target = sys.argv[1]
    if not validateIP(target):
        printErr("Invalid IP Address")
        printUsage()
        sys.exit(2)
    # Validate Logging #
    printHeader(target)
    if not prepareFolder(target):
        sys.exit(2)
    # Begin #
    try:
        services = conductLightNmap(target)
        dispatchModules(target, services)
        conductHeavyNmap(target)
    except KeyboardInterrupt:
        print "\n\nExiting.\n"
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv[1:])
