# OSCP
Scripts I developed to help complete the OSCP certification.

[About]:
The following scripts were designed for use with the Offensive Security
Certified Professional course and exam inside Kali Linux version
4.6.0-kali1-686.

[Warning]:
These scripts come as-is with no promise of functionality or accuracy.  I
strictly wrote them for personal use.  I have no plans to maintain updates,
I did not write them to be efficient and in some cases you may find the
functions may not produce the desired results so use at your own
risk/discretion. I wrote these scripts to target machines in a lab
environment so please only use it against systems for which you have
permission!!  

HACKDELI.PY + HACKDELIDATA.XML
------------------------------

* MOVED TO SEPARATE RESPOSITORY: https://github.com/johneiser/HackDeli

(!) Now hosted at https://johneiser.github.io/HackDeli

REMOTERECON.PY
--------------
Inspired by Mike Czumak's reconscan.py

This script conducts full reconnaissance on a target using three steps:
1. Light NMAP scan to identify services
2. Modular enumeration for each service using tools built into Kali Linux
3. Heavy NMAP scan

The results are stored in text files in the local directory under a new
folder named after the target ip.

(!) Use ctrl+c to skip modules.

Note: Occasionally, when errors have occurred, typing in the terminal is
disabled after running.  I don’t know why this happens, just close the
terminal and open a new one.

Usage: python remoterecon.py <target ip>
Example: python remoterecon.py 192.168.1.12


CREDCHECK.PY
------------
This script checks a set of credentials against a set of IP addresses looking
for valid remote login access using two steps:
1. Light NMAP scan to identify services
2. Modular brute force for each service using tools built into Kali Linux

(!) Use ctrl+c to skip modules.

Usage: python credcheck.py <user|user-file> <pass|pass-file> <target|target-file>
Example: python credcheck.py scott passwords.txt 192.168.1.12


HASHCHECK.PY
------------
This script checks a set of Windows NTLM hashes against a set of IP addresses
looking for valid Pass-The-Hash access.

Hashes must be in the following format:
  USER:ID:LM:NT:::

(!) Use ctrl+c to skip hashes.

Usage: python hashcheck.py <hash|hash-file> <target|target-file>
Example: python hashcheck.py "UserA:501:NO PASSWORD********:A3B93…" targets.txt


XPLOITDELI.PY
-------------
This script produces a variety of exploits found on exploit-db for
immediate use.  Running without the exploit id will list all exploits.

Note: Some options don’t compile, labeled with an asterisk.

Usage: python xploitdeli.py [exploit id]
Example: python xploitdeli.py 1


SQLDELI.PY
-----------
This script will parse the XML file used to power the sqlmap application
built into Kali Linux and present the contents in an interactive menu.

Note: You might need to configure the script to point to the proper location
of sqlmap/xml/queries.xml.

Usage: python sqldeli.py
